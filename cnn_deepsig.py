# import python dependencies

# Ensure that the least loaded GPU is used
# import setGPU

# Plotting Includes
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style("whitegrid")

# External Includes
import numpy as np
from pprint import pprint


# Internal Includes
from rfml.data import Dataset, Encoder
from rfml.data.converters import load_RML201610A_dataset

from rfml.nbutils import plot_acc_vs_snr, plot_confusion, plot_convergence, plot_IQ

from rfml.nn.eval import compute_accuracy, compute_accuracy_on_cross_sections, compute_confusion

from my_cnn import MyCNN
from my_training_strategy import MyTrainingStrategy

# configuration
gpu = True       # Set to True to use a GPU for training
fig_dir = None   # Set to a file path if you'd like to save the plots generated
# data_path = None # Set to a file path if you've downloaded RML2016.10A locally
data_path = "RML2016.10a_dict.pkl"


# loading dataset
dataset = load_RML201610A_dataset(path=data_path)
print(len(dataset))
pprint(dataset.get_examples_per_class())

# drop rows where dolumn does not equal real, adversarial
print(dataset.df['Modulation'].unique())
modules = ["8PSK","AM-DSB","AM-SSB","BPSK","CPFSK","GFSK","PAM4","QAM16","QAM64","QPSK","WBFM"]
for module in modules[0:len(modules)-2]:
    dataset.df.drop( dataset.df[ dataset.df['Modulation'] == module ].index , inplace=True)
print(dataset.df['Modulation'].unique())
print(len(dataset))
pprint(dataset.get_examples_per_class())
modules=["QPSK","WBFM"]

# split data
train, test = dataset.split(frac=0.3, on=["Modulation", "SNR"])
train, val = train.split(frac=0.05, on=["Modulation", "SNR"])

le = Encoder(
    modules,
    label_name="Modulation")
print(le)

# Plot a sample of the data
# You can choose a different sample by changing
idx = 10
snr = 18.0
# modulation = "8PSK"
modulation = "QPSK"

mask = (dataset.df["SNR"] == snr) & (dataset.df["Modulation"] == modulation)
sample = dataset.as_numpy(mask=mask, le=le)[0][idx,0,:]
t = np.arange(sample.shape[1])

title = "{modulation} Sample at {snr:.0f} dB SNR".format(modulation=modulation, snr=snr)
fig = plot_IQ(iq=sample, title=title)
if fig_dir is not None:
    file_path = "{fig_dir}/{modulation}_{snr:.0f}dB_sample.pdf".format(fig_dir=fig_dir,
                                                                       modulation=modulation,
                                                                       snr=snr)
    print("Saving Figure -> {file_path}".format(file_path=file_path))
    fig.savefig(file_path, format="pdf", transparent=True)
plt.show()

#Create NN model
# Image(filename="_fig/CNNDiagramSmall.png")

# print model
model = MyCNN(input_samples=128, n_classes=11)
print(model)

trainer = MyTrainingStrategy(gpu=gpu,n_epochs= 3)
print(trainer)

# putting it all together
train_loss, val_loss = trainer(model=model,
                               training=train,
                               validation=val,
                               le=le)

title = "Training Results of {model_name} on {dataset_name}".format(model_name="MyCNN", dataset_name="RML2016.10A")
fig = plot_convergence(train_loss=train_loss, val_loss=val_loss, title=title)
if fig_dir is not None:
    file_path = "{fig_dir}/training_loss.pdf"
    print("Saving Figure -> {file_path}".format(file_path=file_path))
    fig.savefig(file_path, format="pdf", transparent=True)
plt.show()

# Testing the trained model

acc = compute_accuracy(model=model, data=test, le=le)
print("Overall Testing Accuracy: {:.4f}".format(acc))

acc_vs_snr, snr = compute_accuracy_on_cross_sections(model=model,
                                                     data=test,
                                                     le=le,
                                                     column="SNR")

title = "Accuracy vs SNR of {model_name} on {dataset_name}".format(model_name="MyCNN", dataset_name="RML2016.10A")
fig = plot_acc_vs_snr(acc_vs_snr=acc_vs_snr, snr=snr, title=title)
if fig_dir is not None:
    file_path = "{fig_dir}/acc_vs_snr.pdf"
    print("Saving Figure -> {file_path}".format(file_path=file_path))
    fig.savefig(file_path, format="pdf", transparent=True)
plt.show()

cmn = compute_confusion(model=model, data=test, le=le)

title = "Confusion Matrix of {model_name} on {dataset_name}".format(model_name="MyCNN", dataset_name="RML2016.10A")
fig = plot_confusion(cm=cmn, labels=le.labels, title=title)
if fig_dir is not None:
    file_path = "{fig_dir}/confusion_matrix.pdf"
    print("Saving Figure -> {file_path}".format(file_path=file_path))
    fig.savefig(file_path, format="pdf", transparent=True)
plt.show()